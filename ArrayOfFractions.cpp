/*=============================================================================
Name: 		Eduardo Ortiz
			Brian X. Leon

e-mail:		eduardo3991@hotmail.com
			brian.leon6@gmail.com
=============================================================================*/

#include "ArrayOfFractions.h"
#include <iostream>
#include <cstdlib>		//random numbers
#include <ctime>		//time function
using namespace std;

//Default Constructor
ArrayOfFractions::ArrayOfFractions()
{
	size = 1;

	array = new Fraction[size];
}

//Constructor that receives a given size
ArrayOfFractions::ArrayOfFractions(int SIZE)
{
	if(SIZE > 0)
	{
		srand(time(NULL));

		size = SIZE;
		array = new Fraction[size];

		for(int c = 0; c < size; c++)
		{
			array[c].setNum(rand() % 10 + 1);
			array[c].setDenom(rand() % 10 + 1);
		}
	}
}

//Destructor to free that memory
ArrayOfFractions::~ArrayOfFractions()
{
	delete [] array;
	
	array = NULL;
}

//Function that prints out the Array to the user
void ArrayOfFractions::printArray()
{
	cout << "{";

	for(int c = 0; c < size - 1; c++)
	{
		array[c].print();

		cout << ", ";
	}

	array[size - 1].print();

	cout << "}";
}

//Implementation of Selection Sort for this particular class
void ArrayOfFractions::sortAscending()
{
	if(size == 1) return;

	//classic swapping variables
	Fraction swapper, minimum;
	int swap_pos;

	for(int i = 0; i < (size - 1); i++)
	{
		minimum = array[i];

		for(int c = i; c < size; c++)
		{
			if(minimum.gt(array[c]))
			{
				minimum = array[c];
				swap_pos = c;
			}
		}

		//Swaps the values to sort
		swapper = array[i];
		array[i] = minimum;
		array[swap_pos] = swapper;
	}
}

//Function that returns the minimum Fraction
//Uses the gt(greater than) function from the Fraction class
Fraction ArrayOfFractions::minFrac()
{
	if(size == 1) return array[size - 1];

	Fraction minimum = array[0];

	for(int c = 1; c < size; c++)
		if(minimum.gt(array[c]))
			minimum = array[c];

	return minimum;
}

//Function that returns the maximum Fraction
//Also uses the gt function from the Fraction class
Fraction ArrayOfFractions::maxFrac()
{
	if(size == 1) return array[size - 1];

	Fraction maximum = array[0];

	for(int c = 1; c < size; c++)
		if(array[c].gt(maximum))
			maximum = array[c];

	return maximum;
}

//Function that sums all of the elements in the array and returns the result
//Uses the add function from the Fraction class
Fraction ArrayOfFractions::sumArray()
{
	if(size == 1) return array[size - 1];

	Fraction accumulator;

	for(int c = 0; c < size; c++)
		accumulator = accumulator.add(array[c]);

	return accumulator;
}
