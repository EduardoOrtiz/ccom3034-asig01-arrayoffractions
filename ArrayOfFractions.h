/*=============================================================================
Name: 		Eduardo Ortiz
			Brian X. Leon

e-mail:		eduardo3991@hotmail.com
			brian.leon6@gmail.com
=============================================================================*/

#ifndef ARRAYOFFRACTIONS
#define ARRAYOFFRACTIONS
#include "Fraction.h"

//more like ArrayOfRandomFractions
class ArrayOfFractions
{
	private:

	Fraction * array;
	int size;

	public:

	ArrayOfFractions();
	ArrayOfFractions(int);
	~ArrayOfFractions();

	void printArray();
	void sortAscending();

	Fraction minFrac();
	Fraction maxFrac();
	Fraction sumArray();
};

#endif
