/*=============================================================================
Name: 		Eduardo Ortiz
			Brian X. Leon
			
e-mail:		eduardo3991@hotmail.com
			brian.leon6@gmail.com
=============================================================================*/

#ifndef FRACTION
#define FRACTION

class Fraction {
	private:
	
	int num, denom;
	
	public:
	
	Fraction();
	Fraction(int, int);
	
	//setters
	void setNum(int);
	void setDenom(int);
	
	//getters
	int getNum();
	int getDenom();
	
	void print();							
	
	Fraction add(const Fraction&) const;
	
	Fraction sub(const Fraction&) const;
	
	Fraction mul(const Fraction&) const;
	
	Fraction div(const Fraction&) const;
	
	bool gt(const Fraction &);				//greater than function
	
	void reduce();							//simplifies the fraction

};

#endif