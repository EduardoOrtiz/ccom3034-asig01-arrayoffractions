/*=============================================================================
Name: 		Eduardo Ortiz
			Brian X. Leon

e-mail:		eduardo3991@hotmail.com
			brian.leon6@gmail.com
=============================================================================*/

#include <iostream>
#include "Fraction.h"
using namespace std;

//this->reduce always reduces the fraction after most operations

//Greatest Common Divisor used on the reduce() function
int gcd(int, int);
int gcd(int a, int b)
{
	if(a % b == 0) return b;

	else return gcd(b, a % b);
}

//Default Constructor
Fraction::Fraction()
{
	num = 0;

	denom = 1;
}

Fraction::Fraction(int a, int b)
{
	num = a;

	denom = b;

	this->reduce();
}

void Fraction::setNum(int a)
{
	num = a;

	(*this).reduce();
}

void Fraction::setDenom(int a)
{
	if(a != 0)
	{
		denom = a;
		
		this->reduce();
	}
}

int Fraction::getNum()
{
	return num;
}

int Fraction::getDenom()
{
	return denom;
}

//Displays the fraction to the user
void Fraction::print()
{
	cout << num << "/" << denom;
}

//Addition for two fractions
Fraction Fraction::add(const Fraction& F) const
{
	Fraction dummy;

	dummy.num = (num*F.denom) + (F.num*denom);

	dummy.denom = denom*F.denom;

	dummy.reduce();

	return dummy;
}

//Subtraction for fractions
Fraction Fraction::sub(const Fraction& F) const
{
	Fraction dummy;

	dummy.num = (num*F.denom) - (F.num*denom);

	dummy.denom = denom*F.denom;
	
	dummy.reduce();

	return dummy;
}

//Multiplications for Fractions
Fraction Fraction::mul(const Fraction& F) const
{
	Fraction dummy;

	dummy.num = num * F.num;

	dummy.denom = denom * F.denom;

	dummy.reduce();

	return dummy;
}

//Division for Fractions
Fraction Fraction::div(const Fraction& F) const
{
	Fraction dummy;

	dummy.num = num * F.denom;

	dummy.denom = denom * F.num;

	dummy.reduce();

	return dummy;

}

//Greater than function for fractions
//compares the left fraction wityh the right fraction
bool Fraction::gt(const Fraction & F)
{
	int num1, num2;

	num1 = num * F.denom;

	num2 = F.num * denom;

	if(num1 > num2)
	{
		return true;
	}

	return false;
}

//The reduce function for fractions
//It simplifies the fraction by using the gcd function on both the numerator
//and the denominator
void Fraction::reduce()
{
	int a = gcd(num, denom);

	num = num/a;

	denom = denom/a;
}