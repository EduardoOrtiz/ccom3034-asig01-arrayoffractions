/*=============================================================================
Name: 		Eduardo Ortiz
			Brian X. Leon

e-mail:		eduardo3991@hotmail.com
			brian.leon6@gmail.com
=============================================================================*/

#include <iostream>
#include "ArrayOfFractions.h"
using namespace std;

int main()
{
	//size used for the Array of Fractions
	int SIZE = 10;

	//The array with the given size
	ArrayOfFractions container(SIZE);

	cout << "Now to work with an Array of Fractions..." << endl << endl;

	container.printArray();				//prints to the user

	container.sortAscending();			//Selection Sort

	cout << endl << endl << "In ascending order: " << endl;

	container.printArray();

	cout << endl << endl << "Its minimum:\t";

	container.minFrac().print();		//Prints the Minimum element

	cout << endl << endl << "Its maximum:\t";

	container.maxFrac().print();		//Prints the Maximum element

	cout << endl << endl << "Its summation:\t";

	container.sumArray().print();		//Prints the Summation

	cout << endl;

	return 0;
}
